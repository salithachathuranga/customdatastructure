package com.nCinga;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class StudentStructure {

    private List<Student> students;
    private static StudentStructure studentStructure;

    public StudentStructure() {
        students = new ArrayList<>();
    }

    public static StudentStructure getInstance(){
        studentStructure = studentStructure == null ? new StudentStructure() : studentStructure;
        return studentStructure;
    }

    public void addStudent(Student student){
        students.add(student);
        students.sort(new StudentIdComparator());
    }

    public void addStudent(int index, Student student){
        students.add(index, student);
        students.sort(new StudentIdComparator());
    }

    public void removeStudent(Student student){
        students.remove(student);
        students.sort(new StudentIdComparator());
    }

    public void removeStudent(int index){
        students.remove(index);
        students.sort(new StudentIdComparator());
    }

    public void printStudents(){
        System.out.println(students);
    }

}
