package com.nCinga;

import java.util.Comparator;

public class StudentIdComparator implements Comparator<Student> {

    public int compare(Student student1, Student student2) {
        return student1.getId() - student2.getId();
    }
}
