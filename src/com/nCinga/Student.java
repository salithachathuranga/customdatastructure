package com.nCinga;

import java.util.Objects;

public class Student implements Comparable<Student>{

    private final int id;
    private final String name;
    private final String degree;

    public Student(int id, String name, String degree) {
        this.id = id;
        this.name = name;
        this.degree = degree;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDegree() {
        return degree;
    }

    public String toString() {
        return "Student{" +
                "id=" + id + ", name='" + name + ", degree='" + degree + '}';
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id &&
                name.equals(student.name) &&
                degree.equals(student.degree);
    }

    public int hashCode() {
        return Objects.hash(id, name, degree);
    }

    public int compareTo(Student student) {
        if(this.id == student.id)
            return 0;
        else
            return this.id > student.id ? -1 : 1;
    }
}
