package com.nCinga;

public class Main {

    public static void main(String[] args) {
        StudentStructure structure = StudentStructure.getInstance();
        Student student1 = new Student(1, "salitha", "IS");
        Student student2 = new Student(2, "charith", "ENTC");
        Student student3 = new Student(3, "yasas", "CS");
        structure.addStudent(student3);
        structure.addStudent(student1);
        structure.addStudent(student2);
        structure.addStudent(3, student2);
        structure.removeStudent(student1);
        structure.removeStudent(0);
        structure.printStudents();

    }
}
